
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

    
/**
 *
 * @author OUZ-XLV
 */
public class NotesAnneeMatiere  extends Action{
    private final static String HISTO_TABLE = "histotableau";
private final static String HISTO_GRAPHIC = "histographique";
public ActionForward execute(ActionMapping mapping,
ActionForm form,
HttpServletRequest request,
HttpServletResponse response) throws Exception {
NotesAnneeMatiereForm f =
(NotesAnneeMatiereForm) form;
INotesDAO notesDAO;
        notesDAO = DAOFactory().getNoteDAO();
Histogramme histo = notesDAO.getHistogramme(f.getNumeroAnnee(),
f.getMatiere() );
request.setAttribute("notes", histo);
if (f.getPresentation().equals("graphic")) {
return mapping.findForward(HISTO_GRAPHIC);
} else {
return mapping.findForward(HISTO_TABLE);
}
}

    private Object DAOFactory() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
}
