
import javax.servlet.http.HttpServletRequest;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author OUZ-XLV
 */
public class NotesAnneeMatiereForm extends ActionForm{
 //DECLARATION DES VARIABLES   
 private String annee;
 private String presentation;
 private String matiere;
    private int numeroAnnee;
 //CREATION DES GETTERS ET SETTERS
 public String getAnnee(){
  return annee;   
 }
 public void setAnnee(String annee){
   this.annee=annee;
 }
 public String getPresentation() {
   return presentation;  
 }
 public void setPresentation(String presentation){
  this.presentation=presentation;   
 }
 public String getMatiere(){
 return matiere;    
 }
 public void setMatiere(String matiere){
 this.matiere=matiere;    
 }
 public ActionErrors validate(ActionMapping mapping,
HttpServletRequest request) {
ActionErrors errors = new ActionErrors();
if (annee == null || annee.length() < 1) {
errors.add("Année", new ActionMessage("error.annee.required"));
}
else {
try {
numeroAnnee = Integer.parseInt(annee);
} catch (NumberFormatException e) {
errors.add("Année", new ActionMessage("error.annee.isNotANumber"));
}
}
if ((!matiere.equals("graphic")) && ((!matiere.equals("tableau")) {
errors.add("Année", new ActionMessage("error.annee.required"));
}
return errors;
}
}




 